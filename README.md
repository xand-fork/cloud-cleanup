This repository is for holding CI/CD jobs and scripts that delete cloud resources on a schedule.

Currently it contains scripts that can delete everything deployed in each cloud when using ansible. 

The scripts work on a prepend name that is intended to be filled out when a deployment is made in Ansible using the variable "resource_name_prefix" in each cloud's yml file. The variable $PREPEND should be updated with whatever that value is (in the future, it would make sense to turn this into a variable.

WORK TO GO:

The three scripts need to be modified to accept an amount of time to wait until deletion and the logic to only delete resources older than X amount of time, and then a cron CI/CD job needs to be set up to run them.
